    /**
     * Création du tableau chatMessages
     */
    var chatMessages =
    [
        {
            userPseudo : 'user1',
            message : 'premier message de user1',
            date : new Date().getTime(),
            id : Math.random()
        },
        {
            userPseudo : 'user2',
            message : 'premier message de user2',
            date : new Date().getTime(),
            id : Math.random() 
        }
    ];

    var saved = []; // initialisation du tableau de sauvegarde des messages après le 5eme message

    /**
     * début du chat. demande du pseudo et du message
     */
    function chat()
    {
        var userName = prompt('Veuillez saisir votre pseudo : ');
        var message = prompt('Indiquez votre message : ');
        var idMessage = Math.random();
        var dateMessage = new Date().getTime();

        chatMessages.push({userPseudo : userName, message : message, date : dateMessage, id : idMessage});

        if(chatMessages.length > 4)
        {
            saved.push(chatMessages[chatMessages.length-1]);        // à partir de 5 éléments dans le tableau chatMessages[], le dernier élément se place dans le tableau saved[]. 
            chatMessages.splice(0, 0, saved[saved.length-1]);       // le dernier élément du tableau saved[] se place au début du tableau chatMessages[]
            chatMessages.pop(chatMessages.length-1);          // le dernier élément du tableau chatMessages[] est supprimé
        }

        if(chatMessages.length < 12)
        {
            chat(userName, message);                        // récursion de la fonction
        }
        else
        {
            console.log(chatMessages);                      // affiche le contenu du chat au bout de 12 messages
        }
    }

    chat();

    console.log('----------');
    var conversation = '';                                  // initialisation de la variable qui contiendra la conversation
    var conversationArray = [];                             // initialisation du tableau qui contiendra la conversation

    for(var i = 0; i < chatMessages.length; i++)
    {
        conversation += 'Le ' + new Date(chatMessages[i].date).toUTCString() + ', ' + chatMessages[i].userPseudo + ' a écrit ' + '"' + chatMessages[i].message + '".\n' ;
        conversationArray[i] = 'Le ' + new Date(chatMessages[i].date).toUTCString() + ', ' + chatMessages[i].userPseudo + ' a écrit ' + '"' + chatMessages[i].message + '".\n' ;
    }
    console.log(conversation);

    /**
     * Affichage dans le DOM
     */

    var body = document.body;
    var h1 = document.createElement("h1");      // création de l'élément h1
    var div = document.createElement("div");    // création de la div
    var ul = document.createElement("ul");      // création du ul

    for(var j = 0; j < conversationArray.length; j++)   // boucle qui ajoute un li dans le ul. ce li contient la conversaion
    {
        ul.innerHTML += "<li> " + conversationArray[j] + " </li>" ; 
    }

    h1.textContent = "Chat dans la console et dans le DOM";     // ajout du texte dans la balise h1
    body.append(h1, div);                                       // placement du h1 et de la div dans le body

    div.style.border = "2px solid black";                       // ajout de la bordure à la div 
    div.style.padding = "5px";                                  // ajout du padding à la div
    div.style.borderRadius = "5px";                             // arrondi la bordure de la div

    div.append(ul);                                             // placement du ul dans la div

    var getli = document.getElementsByTagName("li");            // récupère tous les li de la page

    for(var k = 0; k < getli.length; k++)                       // boucle qui change l'arrière plan des li une fois sur deux
    {
        if(k %2 == 0)
        {
            getli[k].style.background = "red";                  // ajoute le style background : red
        }
        else
        {
            getli[k].style.background = "black";                // ajoute le style background : black
            getli[k].style.color = "white";                     // ajoute le style color : white
        }
    }
